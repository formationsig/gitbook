Écrire, Corriger et Diffuser les supports de formation avec Gitlab
======
ceci est un **document en cours de rédaction**  rassemblant les manipulations courantes et les informations minimales pour s'en sortir avec gitlab à l'usage des formateurs.

Toute la documentation est sous licence [![licence CC](images/CC.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

{% hint style='danger' %}

:warning:  **Ceci n'est qu'un test "grandeur nature"**, ayant pour objectif d'expliquer et de démontrer par l'exemple la possibilité d'utiliser gitlab comme dépôt et support de diffusion pour des formations internes à l'Inrap.

{% endhint %}

# GitLab ?

**GitLab** ![gitlab](images/gitlab.png) est un gestionnaire de projets de développements collaboratifs. Grâce à un système de synchronisation **chaque participant à un projet peut avoir une version locale du projet, travailler dessus puis le fusionner/mettre à jour la version en ligne du projet**.

Conçu au départ pour héberger du code informatique, il peut aussi servir de dépôt de sources (images et textes) qui constituent les supports de formations. Ceux-ci deviennent très simplement accessibles en lignes,  et les mises à jour peuvent être mises à dispositions en temps réel.

:star: En outre,  grâce a une "extension" intégrée à gitlab il est possible **à chaque mise à jour d'un projet de générer automatiquement un site web et un document PDF**.

> Ce type de mise en ligne de supports de formation avec génération de pdf est de plus en plus utilisé. un exemple parmi d'autres: [formations R du ministère de la Transition écologique et de la solidarité ](https://mtes-mct.github.io/parcours-r/)



# Le groupe formation sur gitlab.com et les projets

Un groupe [**formationSIG**](https://gitlab.com/formationsig) a été créé sur **gitlab.com** et **contient des projets** concernant les formations liées au déploiement des SIG et des Statistiques à l'Inrap.

![depot_gitlab](images/depot_gitlab.png)



**Ce dépôt est constitué de plusieurs projets, un par formation**, qui contiennent tous les fichiers nécessaires à la publication du support de formation:

* des fichiers de textes mis en pages avec le langage balisé simple **Markdown**:

  * le fichier README.md qui contient l'introduction
  * le fichier SUMMARY.md qui contient la table des matières
  * le ou les fichiers contenant le support de formation

* les images du support dans un dossier [images]

* (éventuellement) les données de la formation

* 2 fichiers de paramétrages de la mise en ligne des supports:

  > Ces fichiers ne sont nécessaires que si l'on souhaite effectuer une mise en ligne.

  * le fichier **.gitlab-ci.yml** contenant les tâches que gitlab va exécuter pour effectuer la mise ne ligne à chaque mise à jour du projet.
  
  * le fichier **book.json** qui permet de paramétrer finement le rendu de la mise en ligne et du PDF (couverture, tables des matières, etc)
  
    

:star: **Il faut retenir que chaque formation a donc**:

*  **son projet dans le dépôt gitlab** dans lequel son déposés tous les documents nécessaires au support. *Par exemple les documents ayant servis a constituer le site que vous consultez actuellement se trouvent à l'adresse: https://gitlab.com/formationsig/gitbook*
* **un adresse de site web** générée automatiquement. *Par exemple vous consultez actuellement l'adresse https://formationsig.gitlab.io/gitbook/* et vous pouvez télécharger le PDF en cliquant en haut de la page sur ![pdf_download](images/pdf_download.png)

:star: **Aussi, une page servant de portail a tous les supports de formations mis en ligne a été créé: https://formationsig.gitlab.io/gitbook/**

{% hint %}

**Pourquoi gitlab.com et pas gitlab.inrap.fr :interrobang: **

La DSI de l'Inrap a mis en place en 2019 une installation de Gitlab propre à l'Inrap qui est d'ors et déjà accessible depuis le réseau Inrap. Cependant cet hébergement nécessite encore quelques paramétrages du côté serveur de l'inrap, pour que gitlab.inrap.fr soit totalement opérant: le dépôt fonctionne mais pas la transformation en site web statique et la création automatique de pdf.

{% endhint %}



#  Comment rédiger les supports de formation ?

Il faut rédiger les supports de formation avec le langage balisé simple **Markdown**. Si cela peut paraître décontenançant au départ, l'apprentissage et très rapide.
Pour commencer il suffira:

- de **télécharger un logiciel d'édition Markdown** (le bloc-note pourrait suffire) qui permet de visualiser en direct la mise en page obtenue.

  *Ce document a été rédigé avec le logiciel [**typora**](https://typora.io/) minimaliste, efficace et permettant d'exporter sous divers formats (PDF, DOCX, entre autres)*

- de se lancer dans la rédaction avec l'aide d'un tutoriel. *par exemple en consultant le [chapitre suivant](pages/markdown_base.md) de cette documentation ou en tapant **markdown** dans un moteur de recherche.*

:star: Rédiger en **markdown** (même en dehors de gitlab) devient en quelques heures, un automatisme et permet de diffuser des documents avec une mise ne page simple et efficace, respectant les règles typographiques et ce, sous de nombreux formats.

#  Comment créer/synchroniser version locale et version en ligne ?

Il faudra d'abord télécharger le logiciel [**git**](https://gitforwindows.org/) pour pouvoir créer, cloner et/ou synchroniser un projet local avec la version en ligne déposée sur  **gitlab**.

Encore une fois, l'utilisation du logiciel **git** peut paraître abrupte de prime abord car elle nécessite d'écrire des lignes de commandes. 

![git_bash](images/git_bash.png)



Il n'en est rien ! Il suffit de connaître 6 commandes pour tout faire[^1]:

Au départ on crée un dossier qui contiendra le versions locales des projets: par exemple c:/git/

**A ne faire qu'une fois:** Depuis ce dossier (c:/git/) → clic droit → Git bash here pour ouvrir la fenêtre de commande:

`git clone`  + adresse https du projet (obtenue  via le bouton clone sur gitlab.com) pour "cloner" le projet en ligne sur son ordinateur.

**A chaque changement en local, que l'on veut mettre à jour en ligne** depuis le dossier du projet (c:/git/gitbook/ par ex.) → clic droit → Git bash here:

`git status` pour comparer la version locale et en ligne du projet .

`git pull` pour mettre à jour le projet local depuis la version en ligne

`git add .`   pour ajouter les fichiers modifiés localement a la version en ligne (​ :warning: ne pas oublier les espaces et le point !)

`git commit -m "mon commentaire"` pour commenter les transformations effectuées (obligatoire)

`git push`  pour envoyer les modifs en ligne (éventuellement suivi de l'adresse https du projet si cela ne fonctionne pas sans)

[^1]: note: un tutoriel plus détaillé ét appliqué est en cours de rédaction. il sera intégré a ce document.


:star: le système de synchronisation en ligne de commandes avec le logiciel **git** est très simple et rapide. Il évite le sur-versionnement des fichiers quand un document peut-être modifié par plusieurs personnes.



# Mettre en ligne et diffuser

:star: Le paramétrage de la mise en ligne du **site web statique** et de **la génération automatique de pdf** n'est à faire qu'une fois et **est une tâche complètement indépendante de la rédaction** (et donc qui peut être totalement "*transparente*" pour le rédacteur).

En outre, après la configuration des 2 fichiers **.gitlab-ci.yml** et **book.json** pour un projet, elle se résume presque a un simple copié-collé de ceux-ci à la racine des autres projets !

 Cependant les détails de ces paramétrages et possibilités de finitions sont accessibles dans les 3 derniers chapitres de cette documentation.

-----------------------------

La suite de ce document aborde:

* [Le langage Markdown](pages/markdown_base.md)
* [Le paramétrage de gitlab pour la mise en ligne automatique](pages/Config_gitlab-ci-yml.md)
* [Le paramétrage de gitbook pour affiner la mise en page](pages/gitbook.md)