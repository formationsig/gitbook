# ![sigFT2](images/sigFT2.png) FAKE readme

Fichier README.md pour tester le fonctionnement de **gitbook** via les **gitlab pages**.

* https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/operateurs_sql.html)

:warning: :raised_hand_with_fingers_splayed: :white_check_mark:

Fichier README de remplacement à déclarer dans le fichier **book.json**

```json
{
    "structure": {
        "readme": "Fake_readme.md"
    },
}
```



