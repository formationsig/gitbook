# ![gitbook](images/gitbook.png) Gitbook: paramétrages et tests

Ce chapitre est le résultat de tests sur le fonctionnement de **gitbook** via les **gitlab pages** c'est à dire sur la mise en ligne et la génération de pdf à chaque mis à jour d'un dépôt sur **gitlab**.

{% hint style='danger' %}
**Attention:** A n'utiliser que dans le cas d'utilisation avec gitbook. Par exemple, un export  PDF de ce document depuis un éditeur Markdown risque d'être décevant !
{% endhint %}

{% hint %}
Pour visualiser le résultat:

*  il faut se rendre sur la page https://formationsig.gitlab.io/gitbook/ (probablement celle que vous lisez actuellement)

* **ET** télécharger/Imprimer le pdf, en cliquant sur le lien dédié en haut à gauche

{% endhint %}



## Général

### Liens vers le PDF

 Si on a fait générer une version PDF de notre **gitbook**  , il est "caché ans les tréfonds de votre dépôt gitlab: 



* Pour y accéder, le lien s'écrit sous la forme:

`https://gitlab.com/formationsig/gitbook/-/jobs/artifacts/master/raw/public/gitbook.pdf?job=pages`
→ il faut remplacer **2 fois** `gitbook` par le **nom court** de votre projet.



* Un lien de téléchargement avec icône, comme ceci  [![pdf](images/pdf.png)](https://gitlab.com/formationsig/gitbook/-/jobs/artifacts/master/raw/public/gitbook.pdf?job=pages) s'écrira donc:

```
 [![pdf](images/pdf.png)](https://gitlab.com/formationsig/gitbook/-/jobs/artifacts/master/raw/public/gitbook.pdf?job=pages)
```

* Encore mieux, utiliser le plugin [**"get-book"** ](#un-lien-pour-télecharger-le-pdf)

### Ajouter une couverture

Pour le PDF automatique on peut ajouter une couverture:

* Il suffit de créer une image de 1800x2360 pixels nommée **cover.jpg** et de la copier à la racine du projet.

* On ajoute aussi une vignette (pourquoi?) de 200x262px nommée **cover_small.jpg**    
  
|![cover](../cover_small.jpg)|
|:---:|
|cover_small.jpg|

### Faire des sauts de pages (uniquement rendu PDF)

pour faire des sauts de pages sur le rendu PDF il suffit d'ajouter la ligne suivante ↓

```html
<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>
```



## Plugins

Généralement pour activer un *plugin* gitbook non-natif, il suffit de le déclarer dans le fichier **book.json** dans la rubrique `"plugins": ` et parfois de le paramétrer dans la rubrique `"pluginsConfig": `

```json
{
  "plugins": ["nom_du_plugin"],
  "pluginsConfig": {
    "nom_du_plugin": {
      "parametre1": "mon_choix",
      "parametre2": ["Choix1", "Choix2"]
    }
  }
}
```



{%hint%}
Pour rechercher à quoi sert ou comment paramétrer un plugin il suffit de taper `gitbook nomduplugin` dans un moteur de recherche ou de chercher directement sur le site [npm](https://www.npmjs.com/search?q=keywords:gitbook-plugin)
{%endhint%}

### Un lien pour télecharger le PDF

Grâce au plugin **"get-book"**[^1] [​ :link: ](https://www.npmjs.com/package/gitbook-plugin-get-book) on peut afficher un lien pour télécharger le document en PDF (si il est généré par le fichier **gitlab-ci.yml**).

[^1]: Tests pour le lien de telechargement PDF en haut de page : [gitbook plugin miracle pdf](https://www.npmjs.com/package/gitbook-plugin-miracle_pdf) = pas d'erreur dans le pipeline mais ne marche pas :disappointed_relieved:  , [gitbook plufin pdf multilink](https://www.npmjs.com/package/gitbook-plugin-pdf-multi-link) plante le pipeline à la géneration html et pdf *missing arg* :disappointed:

Il faut déclarer le plugin dans le fichier  **book.json** et ajouter en paramètre `"url": ` le lien (entre guillemets) vers le pdf.

```json
{
  "plugins": ["get-book"],
  "pluginsConfig": {
    "get-book": {
      "url": "URL_TO_BOOK.PDF",
      "label": "Download PDF"
    }
  }
}
```

→ On remplace donc `"URL_TO_BOOK.PDF"` par `"https://gitlab.com/formationsig/gitbook/-/jobs/artifacts/master/raw/public/gitbook.pdf?job=pages"`

### Une table des matières de chaque section

Pour insérer une table des matières pour chaque section

avec le plugin **"page-toc"**[^2] [ :link: ](https://www.npmjs.com/package/gitbook-plugin-page-toc) qu'l suffit de déclarer dans le fichier **book.json**. Pour les paramètres suivre le lien vers le dépôt du plugin.

[^2]: le plugin **"atoc"** [ :link: ](https://www.npmjs.com/package/gitbook-plugin-atoc) a aussi été testé mais il intégrait des caractères chinois. par contre il insérait une *toc* en chaque début de section aussi dans le pdf !

{% hint %} Ne fonctionne que pour le site web et pas dans le pdf. {% endhint %}

### Du code à copier

Du code mieux intégré avec **"copy-code-button"** [ :link: ](https://www.npmjs.com/package/gitbook-plugin-copy-code-button) qui permet d'insérer un bouton [Copy] dans les blocs de codes.

> On vérifie en même temps l'interprétation des langages

* en sql: 

```sql
SELECT * FROM Table WHERE "champ" = valeur
```
* en sqlite:

```sqlite
SELECT * FROM Table WHERE "champ" = valeur
```

→ en déclarant **sqlite** la coloration syntaxique marche moins bien.. voire pas du tout !



### Des émoticônes

Des emojis/émoticônes avec le plugin **advanced-emoji** [ :link: ](https://github.com/codeclou/gitbook-plugin-advanced-emoji/blob/pdf-test-book/README.md)

| `code:` | **emoji**         |
| :--------- | :--------------: |
| **signalétique** |  |
| `:warning:`| :warning:        |
| `:exclamation:` | :exclamation: |
| `:grey_exclamation:` | :grey_exclamation: |
| `:bangbang:` | :bangbang: |
| `:question:` | :question: |
| `:grey_question:` | :grey_question: |
| `:interrobang:` | :interrobang: |
| `:white_check_mark:` | :white_check_mark: |
| `:heavy_check_mark`: | :heavy_check_mark: |
| `:x:` | :x: |
| `:star`: | :star: |
| `speech_balloon:` | :speech_balloon: |
| `:mag:` | :mag: |
| `:heavy_plus_sign`: | :heavy_plus_sign: |
| `:memo:` | :memo: |
| `:pencil2:` | :pencil2: |
| `:bulb:` | :bulb: |
| `:no_entry:` | :no_entry: |
| `:no_entry_sign:` | :no_entry_sign: |
| `:construction:`   | :construction:   |
| `:checkered_flag:` | :checkered_flag: |
| **animaux** |  |
| `:rabbit:` | :rabbit:         |
| `:snake:`  | :snake:          |
| `:hamster:` | :hamster:        |
| `:bear:` | :bear:         |
|`:fish:`| :fish: |

→ l'interprétation des emoticônes par le plugin **emoji-advanced** semble hyper sensibles aux espaces: **il ne faut qu'aucun caractère ne les encadrent directement** sinon ils ne sont pas interprétés:
* ici je veux un :panda_face:
* alors que là je n'aurai pas de:panda_face: ! 
* → cqfd

:exclamation::grey_exclamation::warning: → par contre ils peuvent se suivre sans espace ?



 pour avoir une belle [liste d'exemples](https://www.webfx.com/tools/emoji-cheat-sheet/) d’émoticônes (il suffit de cliquer sur celui que vous voulez pour copier son code!)



### Des *hints*

Mettre en avant des informations avec le plugin **"hints"** [ :link: ](https://www.npmjs.com/package/gitbook-plugin-hints) en insérant:

```
{% hint style='info' %}
Important info: this note needs to be highlighted
{% endhint %}
```

{% hint style='info' %}
BLEU - Important info: this note needs to be highlighted - style par défaut = 'info'
{% endhint %}

{% hint style='tip' %}
VERT - Important info - style = 'tip'
{% endhint %}

{% hint style='danger' %}
ROUGE - Important info - style = 'danger'
{% endhint %}

{% hint style='working' %}
JAUNE - Important info - style = 'working'
{% endhint %}

Les icônes par défaut, issus de la bibliothèque de police [Font Awesome](https://fontawesome.com/icons?d=gallery&m=free) peuvent être changés dans le fichier **book.json** en le configurant:
```json
{
    "plugins": ["hints"],
    "pluginsConfig": {
        "hints": {
            "info": "fa fa-info-circle",
            "tip": "fa fa-mortar-board",
            "danger": "fa fa-exclamation-cicle",
            "working": "fa fa-wrench"
        }
    }
}
```
{% hint style='danger' %}

Fonctionne bien pour le site web statique (icône + couleur) et dans l'export PDF (couleur uniquement)
{% endhint %}

{% hint style='tip' %}
Il est même possible d'animer certains icônes comme expliqué sur le site de [Font Awesome](https://fontawesome.com/v4.7.0/examples/#animated)
{% endhint %}

### Ajouter des liens

Pas besoin de plugins particuliers mais une "rubrique" lien dans le **book.json** qui permet:

* d'ajouter un lien perso dans la barre de menu (*sidebar*), par exemple l'Accueil du site.
* des liens de partage de la page

### Intégrer un PDF

avec le plugin **"embed-pdf"** [ :link: ](https://www.npmjs.com/package/gitbook-plugin-embed-pdf) qu'il faut déclarer dans le fichier **book.json**. Puis il suffit d'insérer dans le document Markdown

```
{% pdf src="images/QGIS_raccourcis_clavier_FR.pdf", width="100%", height="650", link=false %}{% endpdf %}
```

qui donne:

{% pdf src="images/QGIS_raccourcis_clavier_FR.pdf", width="100%", height="650", link=false %}{% endpdf %}

{% hint style='danger' %}

**fonctionne avec Chrome mais pas avec Firefox** → il propose alors de télécharger le pdf. Aussi, le pdf n'est pas inclus dans le pdf général, il propose aussi le message en anglais

→ **A n'utiliser qu'avec parcimonie et en connaissance de causes** cad qu pour un usage web et avec Chrome..

{% endhint %}

### Indexation et recherche

Des plugins d'indexation et de recherche sont apparemment activés par défaut.
On peut les désactiver dans le fichier **book.json"**  en les faisant précéder d'un `-` ainsi   `"-lunr", "-search", "search-plus"`

### Insérer un saut de page sur le PDF

Test de [gitbook plugin breakpage](https://www.npmjs.com/package/gitbook-plugin-breakpage-pdf)
<<< est sensé faire un saut de page :interrobang: mais ne fonctionne pas..

en html sinon avec:
```
<div style="page-break-after: always;"></div>
```
→ fonctionne bien, mais c'est mettre du html en plus ..
→ le plus simple reste de faire un document .md par chapitre ou grandes sections. EN effet, chaque document .md commence sur une nouvelle page. 
→ attention je saute la page :scream_cat:  ...

<div style="page-break-after: always;"></div>
... et hop :smile_cat:  !

### A tester/A faire


* tout ce qui est gitbook plugin pdf (saut de page, incrustation pdf,lien pdf a telecharger...)

  → lien vers PDF à télecharger  :heavy_check_mark:

  → PDF embarqué :heavy_check_mark: 

  → saut de page dans le PDF :heavy_check_mark:

* trouver un bon système de table des matières (*toc*)

  → table des matières par section sur la version en ligne :heavy_check_mark:

* améliorer la *sidebar* (toc interne + lien vers toc général + auteurs + logo)

  → lien vers page d'ACcueil en haut de la *sidebar* :heavy_check_mark:

* vérifier les *header* & *footer* (avec auteurs + mini CC + date ?) → parce que 

```json
"pdf": {
        "headerTemplate": "_AUTHOR_ - _PAGENUM_",
        "footerTemplate": "_TITLE_ - _SECTION_"
    }
```

ne semble pas fonctionner !

→ cf. [local plugin page footer](https://www.npmjs.com/package/gitbook-plugin-local-pagefooter-pdf) pour ajouter date en bas de page

→ test de [plugin-local-pagefooter-pdf](https://www.npmjs.com/package/gitbook-plugin-local-pagefooter-pdf) qui permet d'ajouter la date de génération du document (+ message *powered by gitbook*)**uniquement** sur la page web (pas le pdf)

→ test de [gitbook-plugin-gitlab-footer](https://www.npmjs.com/package/gitbook-plugin-gitlab-footer) idem mais sans le message et plus de possibilités (trop? + horodatage en UTC !)

* essayer les plugins de quizz /exercices voir de spoiler
* comprendre GLOSSARY.md qui ne fonctionne pas non plus.
* Apparemment on peut aussi simuler les touches du clavier en entoutrant des caractères avec `<kbd>`Alt`</kbd>`qui donnera <kbd>Alt</kbd>

-----------------------------------------
:fast_forward: [Configuration du fichier book.json](Config_bookjson.md)
