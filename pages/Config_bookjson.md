# ![gitbook](images/gitbook.png) Configuration du fichier book.json

Pour paramétrer au mieux un **gitbook** -généré via gitlab pages ou non, d'ailleurs- cela se passe dans un fichier **book.json** qu'il faut copier à la racine du projet a publier (avec les fichiers .gitlab-ci.yml, README.md et SUMMARY.md)

Cela permet:

* de décrire les métadonnées du gitbook
* de définir la structure du document (les fichier .md a mettre en œuvre)
* de définir et configurer des extensions/*plugins* et de les paramétrer si besoin
* de paramétrer les sorties de gitbook:
  * de manière générale
  * spécifiquement pour la sortie PDF

### Exemple de fichier book.json

```json
{
    "title": "Gitbook - Mode d'emploi",
	"author": "équipe Formateurs & Référents SIG",
	"description": "fake-document pour tester gitbook integré à gitlab, nottament les plugins",
	"license": "CC BY-NC-SA 3.0 FR",
	"language": "fr",
	"repo": "https://gitlab.com/formationsig/gitbook",
    "structure": {
        "readme": "README.md"
    },
    "plugins": [
		"advanced-emoji",
		"copy-code-button",
		"get-book",
		"embed-pdf",
		"local-pagefooter",
		"page-toc",
        "-lunr", "-search", "search-plus",
        "hints",
        "livereload"        
    ],
    "pluginsConfig": {
		"get-book": {
			"url": "https://gitlab.com/formationsig/gitbook/-/jobs/artifacts/master/raw/public/gitbook.pdf?job=pages",
			"label": "Télécharger le PDF"
		},
		"page-toc": {
			"selector": ".markdown-section h1, .markdown-section h2, .markdown-section h3, .markdown-section h4",
            "position": "before-first",
			"showByDefault": true
		}, 
		"local-pagefooter": {
			"islocal": true,
			"modify_label": "Inrap - équipe Formateurs & Référents SIG - document généré le ",
			"modify_format": "DD-MM-YYYY HH:mm:ss"
		}
    },
	"links": {
        "sidebar": {
            "Accueil - Formations SIG": "https://formationsig.gitlab.io/toc/"
        },
		"sharing": {
            "all": ["twitter", "facebook"],
            "facebook": false,
            "google": false,
            "twitter": false
		}
	},
	"pdf": {
        "margin": {
            "left": 56,
            "right": 56,
            "top": 56,
            "bottom": 56
        },
        "pageNumbers": false,
        "fontSize": 11,
        "fontFamily": "Roboto",
        "paperSize": "a4",
		"toc": true
    }
}
```

{% hint style='danger' %}
:warning: depuis 2023 le nom de la branche principale dans gitlab est passé de `master` a `main`. En conséquence, pour tout nouveau projet il faut remplacer le premier par le second dans le fichier  .gitlab-ci.yml (ainsi que dans le fichier .gitlab-ci.yml ).
{% endhint %}

{%hint style = 'danger'%}

Le format JSON est très sensible: 

* la moindre , oubliée renverra un message du genre `SyntaxError: /builds/formationsig/gitbook/book.json: Unexpected token } in JSON at position 556`
* aussi si l'on trouve des exemples de JSON avec commentaires (caractères `//`) il ne seront pas acceptés lors du traitement, renvoyant une erreur du même type

{%endhint%}

### Ressources:

* sur le site officiel de [gitbook.io](https://gitbookio.gitbooks.io/documentation/content/format/configuration.html) et pour une explication générale de la structure https://github.com/GitbookIO/gitbook/blob/master/docs/config.md

* une bonne ressource, détaillée et en espagnol .. **PERDUE**

**exemples divers**:

* des exemples chinois de **bookjson** sur http://gitbook.zhangjikai.com/bookjson.html et https://janicezhw.github.io/gitbook/startusegitbook/configInfo/bookjson.html

---------------------
:leftwards_arrow_with_hook: [Retour à l'introduction](../README.md)
