# ![md](images/md.png) Le langage Markdown : les bases

La documentation présente dans GitLab est écrite en [**markdown**](https://fr.wikipedia.org/wiki/Markdown) un langage de balisage avec une syntaxe relativement simple permettant d'écrire très simplement un texte avec une mise en forme minimale. les fichiers enregistrés ont l'extension **.md**

Pour commencer téléchargez un logiciel d'édition qui permet de visualiser le résultat de la mise en page directement tel que [**typora**](https://typora.io/) par exemple. 

>  Dans ce logiciel vous pouvez passer du mode "code source" dans lequel vous voyez les caractères balises au mode "mise en page" dans lequel vous voyez le résultat. (*dans **typora** il suffit de cliquer en bas de la page sur*`</>`)



## Mise en bouche

Avec le langage **markdown** on peut très simplement:

* faire des niveaux de titre:

># Titre 1
>
>## Titre 2
>
>### Titre 3
>
>```
># Titre 1
>## Titre 2
>### Titre 3
>````

* Mettre des mots en **gras** et en *italique* voir ~~barrés~~

> ````
> Mettre des mots en **gras** et en *italique* voir ~~barrés~~
> ````

* Faire des listes:

> * point 1
> * point 2
>   * point 2.1
>   * point 2.2
>
> 1. d'abord
> 2. ensuite
> 3. enfin
>
> ````
> * point 1
> * point 2
> 	* point 2.1
> 	* point 2.2
> 	
> 1. d'abord
> 2. ensuite
> 3. enfin
> ```

* insérer des liens vers [un super blog](https://archeomatic.wordpress.com/)

> ```
> insérer des liens vers [un super blog](https://archeomatic.wordpress.com/)
> ```

* ou insérer une image  ![icon_gitlab](images/gitlab.png)

> ```
> ou insérer une image ![icon_gitlab](images/gitlab.png)
> ```

note: Les images sont insérées grâce a un simple lien. Ainsi il suffit de remplacer l'image en gardant le même nom de fichier pour qu'elle se modifie dans tous les documents qui font appel à celle-ci !

* et même des tableaux !

>| ID   | nom     | prénom  | taille |
>| ---- | ------- | ------- | ------ |
>| 1    | braudel | fernand | 165    |
>| 2    | bertin  | jacques | 183    |
>
>```
>| ID   | nom       | prénom | taille |
>| ---- | --------- | ------ | ------ |
>| 1    | braudel | fernand   | 165     |
>| 2    | bertin    | jacques | 183    |
>```



Un peu déconcertant les premières minutes... on se met très vite à prendre du plaisir à écrire et mettre en page juste avec un clavier !

Bon d'accord si vous insistez on peut mettre des émoticônes chat :cat: , voire des têtes de mort ! :skull: à vous de trouver comment [?!](https://gist.github.com/rxaviers/7360908)

> Par contre ils peuvent être sensibles :disappointed: : aucun caractère ne doit les toucher !

{% hint %}

il existe  sur la toile,  de multiples ressources pour expliquer la syntaxe Markdown en voici une: https://docs.framasoft.org/fr/grav/markdown.html#

{% endhint %}

-------------------------------------

:fast_forward: [Le langage Markdown - avancé](markdown_avance.md)