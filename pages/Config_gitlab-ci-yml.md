# ![gitlab_logo](images/gitlab_logo.png) Configuration du fichier .gitlab-ci.yml

Pour que **gitlab pages** puisse générer un **gitbook** à partir de vos documents en Markdown il faut avoir un fichier **.gitlab-ci.yml** (à la racine du projet dans le dépôt gitlab) qui sera exécuté à chaque **`git push`**.

{%hint%}

Son exécution pourra être surveillée (en cours d’exécution ) ou analysée (en cas d'erreur) via le repo gitlab du projet depuis le sous-menu **Intégration et livraison continues** → **Pipeline**

{%endhint%}



Ce fichier permet:

* de définir des  étapes de traitements à exécuter par des machines virtuelles dans gitlab

* pour ce qui nous intéresse ici, la transformation via **gitbook** de nos fichiers Markdown en **site web statique** et en **PDF**

  

### Exemple de fichier .gitlab-ci.yml

```yml
 # This pipeline run three stages Test, Build and Deploy
stages:
  - test
  - build
  - deploy

image: goffinet/gitbook:latest

# the 'gitbook' job will test the gitbook tools
gitbook:
  stage: test
  script:
    - 'echo "node version: $(node -v)"'
    - gitbook -V
    - calibre --version
  allow_failure: false

# the 'lint' job will test the markdown syntax
lint:
  stage: test
  script:
    - 'echo "node version: $(node -v)"'
    - echo "markdownlint version:" $(markdownlint -V)
    - markdownlint --config ./markdownlint.json README.md
    - markdownlint --config ./markdownlint.json *.md
  allow_failure: true

# the 'html' job will build your document in html format
html:
  stage: build
  dependencies:
    - gitbook
    - lint
  script:
    - gitbook install # add any requested plugins in book.json
    - gitbook build . book # html build
  artifacts:
    paths:
      - book
    expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch the 'html' job will build your document in pdf format
  allow_failure: false

# the 'pdf' job will build your document in pdf format
pdf:
  stage: build
  dependencies:
    - gitbook
    - lint
  before_script:
    - mkdir ebooks
  script:
    - gitbook install # add any requested plugins in book.json
    - gitbook pdf . ebooks/${CI_PROJECT_NAME}.pdf # pdf build
  artifacts:
    paths:
      - ebooks/${CI_PROJECT_NAME}.pdf
    expire_in: 1 day
  only:
    - master # this job will affect only the 'master' branch the 'pdf' job will build your document in pdf format

# the 'pages' job will deploy your site to your gitlab pages service
pages:
  stage: deploy
  dependencies:
    - html
    - pdf    
  script:
    - mkdir .public
    - cp -r book/* .public
    - cp -r ebooks/* .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```

{% hint style='danger' %}
:warning: depuis 2023 le nom de la branche principale dans gitlab est passé de `master` a `main`. En conséquence, pour tout nouveau projet il faut remplacer toutes les occurrences du premier par le second dans le fichier  .gitlab-ci.yml (ainsi que dans le fichier book.json).
{% endhint %}

### Ressources

Une ressource essentielle, d'ou est tiré l'exemple ci-dessus sur le site [goffinet.org](https://gitlab-ci.goffinet.org/#3-projet-de-d%C3%A9part-gitlab-ci-avec-pages)

-------------------

:fast_forward: [Configuration de gitbook](gitbook.md)