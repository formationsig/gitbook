# Summary

## Gitbook  w/ Gitlab: mode d'emploi

:warning: cette ligne ne sera pas lue....en gros tout ce qui ne sera pas une puce



## Le Langage Markdown

  - [Initiation](pages/markdown_base.md)
  - [Avancé](pages/markdown_avance.md)

  

## Configuration de gitlab

  - [Le fichier .gitlab-ci.yml](pages/Config_gitlab-ci-yml.md)

  

## Configuration de gitbook

  - [Paramètrages et tests](pages/gitbook.md)
  
  - [Le fichier book.json](pages/Config_bookjson.md)
